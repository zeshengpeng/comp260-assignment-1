﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour {
	public int health;
	public int maxhealth=200;
	public bool isRegenHealth = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (health <=0){
			Destroy (this.gameObject);
		}
		if (health < maxhealth && !isRegenHealth) {
			StartCoroutine (RegainHealthOverTime ());
		}
	}


	private IEnumerator RegainHealthOverTime(){
		isRegenHealth = true;
	
	while(health<maxhealth){
			health += 10;
			yield return new WaitForSeconds (1);
	}
		isRegenHealth = false;
	}


	void OnCollisionEnter(Collision collision) {
		GameObject hit = collision.gameObject;
		if (hit.CompareTag("Damage")) {

			health= health - 150;

		}
		if (hit.CompareTag("Fist")) {

			health= health - 10;

		}

		if (hit.CompareTag("Crowbar")) {

			health= health - 50;

		}
	}
}
