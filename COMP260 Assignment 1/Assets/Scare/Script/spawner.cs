﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour {
	public Rigidbody zombie;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter(Collision collision) {
		GameObject hit = collision.gameObject;
		if (hit.CompareTag("Player"))  {
			Rigidbody instantiatedZombie = Instantiate (zombie,
				                              transform.position,
				                              transform.rotation)
			as Rigidbody;
			Destroy (this.gameObject);
		}
	}
}
